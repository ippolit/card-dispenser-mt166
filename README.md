# MT166 Card Dispenser Interface

Copyright &copy;: 2023-2024 WWV. All rights reserved.
Version: 2.0.1

## Table of contents

* [MT166 Card Dispenser](#mt166-card-dispenser)
  * [Install and Run](#install-end-run)
    * [Systemd Service Configuration](#systemd-service-configuration)
  * [Features](#features)
  * [Endpoints](#endpoints)
  * [Request and Response Formats](#request-and-response-formats)
  * [Example Usage](#example-usage)
    * [Get Dispenser version](#get-dispenser-version)
    * [Retrieve Card Position](#retrieve-card-position)
    * [Retrieve MT166 Status](#retrieve-mt166-status)
  * [MT166 Sequence Diagram](#mt166-sequence-diagram)
  * [Dependencies](#dependencies)

---

## MT166 Card Dispenser

This document provides a comprehensive guide on the communication protocol between a Python Flask-based
interface and the MT166 device. It details the sequence of data transmission, clarifying how the Flask
application initiates the exchange, manages the flow of information, and ensures accurate data handling
throughout the process. This includes establishing a connection, transmitting 8-bit data payloads,
and appropriately terminating the communication with well-defined start and stop bits.
Each step of the interface's interaction with the MT166 device is meticulously outlined to facilitate
developers in understanding and implementing the necessary procedures for successful data exchange.

### Install end Run

This section provides instructions on how to set up and run the MT166 Card Dispenser application.
Initially, create a Python virtual environment named 'MT166' to maintain dependencies separately.
Activate this environment using the source command. Next, install all required dependencies from
the 'requirements.txt' file to ensure all necessary libraries are available. Finally, set the environment
variable 'FLASK_APP' to the main application file 'MT166-dispenser.py' and start the Flask server
to run the application. This procedure is essential for a successful installation and execution
of the MT166 software.

```bash
python3 -m venv venv_MT166
source venv_MT166/bin/activate
pip install -r requirements.txt
FLASK_APP=MT166-dispenser.py FLASK_RUN_PORT=8003 flask run
```

#### Systemd Service Configuration

To ensure that this interface starts automatically, create a systemd script. This script will enable
the service at system boot. Below is an example configuration for the MT166 API Service.

##### Example Configuration

Add the following systemd configuration to a new file in your system:

File: `/usr/lib/systemd/system/mt166-interface.service`

```bash
[Unit]
Description=MT166 API Service
After=network.target
StartLimitIntervalSec=500
StartLimitBurst=5

[Service]
User=mt166user
Group=mt166user
WorkingDirectory=/opt/mt166user
StandardOutput=journal
ExecStart=FLASK_APP=MT166-dispenser.py FLASK_RUN_PORT=8003 flask run
Restart=always
RestartSec=5s

[Install]
WantedBy=multi-user.target
```

##### Enabling and Starting the Service

After adding the configuration file, enable and start the service using the following commands:

```bash
systemctl enable mt166-interface.service
systemctl start mt166-interface.service
```

This will configure the service to start automatically at system boot and ensure that it runs underthe specified conditions.

### Features

1. **Command Transmission**: Send specific command sets to control the MT166 device operations.
2. **Response Handling**: Receive and decode data from the MT166 device, handling standard and error responses.
3. **Comprehensive Error Handling**: Manage communication interruptions and log erroneous responses for troubleshooting.
4. **Endpoint Accessibility**: Utilize Flask routes to interact with the MT166 functionalities through HTTP requests.

### Endpoints

* **GET /version**: Fetch the current firmware version of the MT166 device.
* **GET /status**: Obtain various status indicators from the MT166 device.
* **GET /card_find**: Searches for a card within the MT166 device and reports its presence.
* **GET /card_serial**: Retrieves the serial number of the Mifare card present in the MT166 device.
* **POST /card_position**: Set postion of teh card(read, bazel, recycling)
* **POST /verify_password**: Submit a password for verification against a specified sector.
* **POST /change_password**: Change the access password for a particular sector.
* **POST /read_block**: Read a block of data from a specified sector and block number.
* **POST /write_block**: Write a block of data to a specified sector and block number.

### Request and Response Formats

* **Request Format**: API endpoints accept HTTP GET for retrieval operations and POST for actions requiring
  data submission, such as password verification and data writing.
* **Response Format**: Responses are structured in JSON, providing detailed success indicators or descriptive
  error messages depending on the operation result.

### Example Usage

#### Get Dispenser version

```bash
curl -X GET http://localhost:8003/version
```

Response:

```json
{
  "Version": "MT166 V3.101"
}
```

info: the same structure and answer is for bazel nad recycling

#### Retrieve MT166 Status

```bash
curl -X GET http://localhost:8003/status
```

Response:

```json
{
  "card_hopper_empty": false,
  "card_on_bezel": false,
  "card_on_read_position": false,
  "collecting_cards": false,
  "dispensing_card_error": false,
  "dispensing_cards": false,
  "less_cards": false,
  "overtime_recycle_function": false
}
```

#### Retrieve Card Position

```bash
curl -X POST http://localhost:8003/card_position \
    -H "Content-Type: application/json" \
    -d '{"position_type": "read"}'
```

Response:

```json
{
  "status": true
}
```

#### Verify Key_A or Key_B password

```bash
curl -X POST http://localhost:8003/verify_password \
  -H "Content-Type: application/json" \
  -d '{"sector_number": 0, "password_type": "B", "password": "FFFFFFFFFFFF"}'
```

Response:

```json
{
  "message": "Password verified successfully.",
  "status": true
}
```

#### Read sector block data

```bash
curl -X POST http://localhost:8003/read_block \
  -H "Content-Type: application/json" \
  -d '{"sector_number": 0, "block_number": 0}'
```

Response:

```json
{
  "data": "596bf71b55d2080400018708722f3cfe",
  "status": true
}
```

#### Change password

```bash
curl -X POST http://localhost:8003/change_password \
  -H "Content-Type: application/json" \
  -d '{"sector_number": 0, "new_keya": "ff00ff00ff00"}'
```

Response:

```json
{
  "message": "Password changed successfully.",
  "status": true
}
```

#### Write sector block data

```bash
curl -X POST http://localhost:8003/write_block \
  -H "Content-Type: application/json" \
  -d '{
    "sector_number": 1,
    "block_number": 0,
    "block_data": "112233445566778899aabbccddeeff00"
  }'
```

Response:

```json
{
  "message": "Block written successfully",
  "status": true
}
```

#### Example error response

```json
{
  "message": "Failed to write block, status code: 34",
  "status": false
}
```

### MT166 Sequence Diagram

```plantuml
@startuml
skinparam responseMessageBelowArrow true
hide footbox

actor User as U
entity Flask_Service as F
database MT166_Device as M

U -> F: GET /<command>
activate F
F -> M: communicate_with_MT166([data])
activate M
M --> F: Response
deactivate M
F --> U: JSON (status)
deactivate F

@enduml
```

### Dependencies

* Flask: Python microframework for web development.
* PySerial: Python library for serial communication.
* Logging: Python module for logging messages.

This detailed guide should adequately equip developers with the necessary information to interact with
the MT166 device, execute commands, and handle responses effectively. The sequence diagrams provide
a visual understanding of the process flows, enhancing comprehension and implementation ease.
