# -*- coding: utf-8 -*-
# Copyright (c): 2023-2024 WWV. All rights reserved.

# Improved imports for clarity and avoiding circular dependencies
from devices import app, logger, ser


def main():
    """
    Main function to start the Flask server for Card Dispenser Interface.
    It also ensures that the serial connection is properly closed on exit.
    """
    logger.info("Starting Flask server for Card Dispenser Interface")
    try:
        app.run()
    except Exception as ex:
        logger.error("An error occurred: %s", ex)
    finally:
        # Ensuring the serial connection is properly closed
        if ser.is_open:
            ser.close()
            logger.info("Serial connection closed")

if __name__ == '__main__':
    main()