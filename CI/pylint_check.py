#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright (c): 2023-2024 WWV. All rights reserved.

from os import path, walk
from re import search
from subprocess import run, PIPE
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter


__description__ = """
This script is used for check all python files in project by pylint
"""
__version__ = "1.0.1"


def check_files_with_pylint(file_list, min_score, config_file):
    """
    Run pylint on a list of Python files and collect those that score below
    a specified minimum score.

    Parameters:
    file_list (list): A list of file paths to Python files to be checked.
    min_score (float): The minimum acceptable pylint score.
    config_file (str): The path to the pylint configuration file.

    Returns:
    tuple: A tuple containing two elements;
        error_files (list): A list of tuples (file path, pylint output) for files 
                            scoring below min_score.
        all_files_scores (list): A list of pylint scores for all checked files.
    """
    error_files = []
    all_files_scores = []
    for file in file_list:
        if not file.endswith('.py'):
            continue

        cmd = f'pylint {file} --rcfile={config_file} --exit-zero'
        result = run(cmd, stdout=PIPE, stderr=PIPE,
                     text=True, shell=True, check=False)
        output = result.stdout
        score_line = [line for line in output.split('\n') if 'Your code has been rated at' in line]

        if score_line:
            try:
                score_match = search(r'rated at ([\d.]+)/10', score_line[0])
                if score_match:
                    score = float(score_match.group(1))
                    print(f'{file}: {score}')
                    all_files_scores.append(score)

                    if score < min_score:
                        error_files.append((file, output))
                else:
                    print(f"Error: Could not find score in line '{score_line[0]}' for file {file}")
            except ValueError as e:
                print(f"Error parsing score for file {file}: {e}")
                print(f"Score line: {score_line[0]}")
                print(f"Output:\n{output}")

    return error_files, all_files_scores


def get_all_python_files(directory):
    """
    Recursively find all Python (.py) files in a given directory.

    Parameters:
    directory (str): The path to the directory in which to search for Python files.

    Returns:
    list: A list of file paths to all Python files found in the specified directory.
    """
    python_files = []
    for root, _, files in walk(directory):
        for file in files:
            if file.endswith('.py'):
                python_files.append(path.join(root, file))
    return python_files


def main(directory, min_score, config_file):
    """
    The main function of the script. It gets a list of all Python files in a given directory,
    checks them with pylint, and prints a summary of the results.

    Parameters:
    directory (str): The path to the directory to be checked.
    min_score (float): The minimum score a file must have to be considered as passed.
    config_file (str): The path to the pylint configuration file.

    Returns:
    None
    """
    file_list = get_all_python_files(directory)
    error_files, all_files_scores = check_files_with_pylint(file_list, min_score, config_file)
    exit_status = 0

    total_files = len(file_list)
    if error_files:
        print('\nErrors found in the following files:')
        for f, errors in error_files:
            print(f'\n{f}:\n{errors}')
        exit_status = 1
    else:
        print(f"\nAll files passed pylint tests with a score of at least {min_score}.")

    if all_files_scores:
        print(f"Total files checked: {total_files}")
        print(f"Files with errors: {len(error_files)}")
        print(f"Files passed: {total_files - len(error_files)}")
        average_score = sum(all_files_scores) / total_files
        print(f"Average score: {average_score:.2f}")

    exit(exit_status)


if __name__ == '__main__':
    parser = ArgumentParser(description=__description__,
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument("-d", dest="directory", type=str, default=".",
                        help="Path to the directory to be checked")
    parser.add_argument("-s", dest="min_score", type=int, default=10.0,
                        help="Minimal score to pass test")
    parser.add_argument("-c", "--config", dest="config_file", type=str, default=None,
                        help="Path to the pylint configuration file")
    parser.add_argument("-V", "--version", action="version", version="%(prog)s VER: " + __version__)

    args = parser.parse_args()

    exit(main(directory=args.directory, min_score=args.min_score,
              config_file=args.config_file))
