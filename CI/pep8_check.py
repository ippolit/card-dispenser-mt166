#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright (c): 2023-2024 WWV. All rights reserved.

from os import path, walk
from subprocess import run, PIPE
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter

__description__ = """
This script is used to check all python files in a project for PEP 8 compliance using flake8
"""
__version__ = "1.0.1"


def check_files_with_flake8(file_list, config_file):
    """
    Check a list of Python files for PEP 8 compliance using flake8.

    Parameters:
    file_list (list of str): A list of file paths to Python files to be checked.
    config_file (str): The path to the flake8 configuration file.

    Returns:
    list of tuple: A list of tuples (file path, flake8 output) for files with PEP 8 compliance issues.
    """
    error_files = []
    for file in file_list:
        if not file.endswith('.py'):
            continue

        cmd = f'flake8 {file} --config={config_file}'
        result = run(cmd, stdout=PIPE, stderr=PIPE,
                     text=True, shell=True, check=False)
        output = result.stdout

        if output:
            error_files.append((file, output))
            print(f'\n{file}:\n{output}')
        else:
            print(f'{file}: OK')

    return error_files


def get_all_python_files(directory):
    """
    Recursively find all Python (.py) files in a given directory.

    Parameters:
    directory (str): The path to the directory in which to search for Python files.

    Returns:
    list of str: A list of file paths to all Python files found in the specified directory.
    """
    python_files = []
    for root, _, files in walk(directory):
        for file in files:
            if file.endswith('.py'):
                python_files.append(path.join(root, file))
    return python_files


def main(directory, config_file):
    """
    The main function of the script. It retrieves a list of all Python files in a given directory,
    checks them with flake8 for PEP 8 compliance, and prints a summary of the results.

    Parameters:
    directory (str): The path to the directory to be checked.
    config_file (str): The path to the flake8 configuration file.

    Returns:
    None: Exits the script with an exit status of 0 (all files are compliant) or 1 (some files are not compliant).
    """
    file_list = get_all_python_files(directory)
    error_files = check_files_with_flake8(file_list, config_file)
    exit_status = 0

    total_files = len(file_list)
    if error_files:
        print('\nErrors found in the following files:')
        for f, errors in error_files:
            print(f'\n{f}:\n{errors}')
        exit_status = 1
    else:
        print("\nAll files passed PEP 8 compliance.")

    print(f"Total files checked: {total_files}")
    print(f"Files with errors: {len(error_files)}")
    print(f"Files passed: {total_files - len(error_files)}")

    exit(exit_status)


if __name__ == '__main__':
    parser = ArgumentParser(description=__description__,
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument("-d", dest="directory", type=str, default=".",
                        help="Path to the directory to be checked")
    parser.add_argument("-c", "--config", dest="config_file", type=str, default=None,
                        help="Path to the pylint configuration file")
    parser.add_argument("-V", "--version", action="version", version="%(prog)s VER: " + __version__)

    args = parser.parse_args()

    exit(main(directory=args.directory, config_file=args.config_file))
