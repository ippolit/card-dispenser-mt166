# -*- coding: utf-8 -*-
# Copyright (c): 2023-2024 WWV. All rights reserved.

import logging
import sys
from unittest import mock
from flask import Flask, jsonify
from flasgger import Swagger
import serial
import json

__description__ = """
MT166 Card Dispenser Interface:
This application provides a simple Flask interface to interact with a Card Dispenser MT166 device over RS232.
"""
version_file = 'version.json'
with open(version_file, 'r') as file:
    data = json.load(file)
    __version__ = data['Version']


app = Flask(__name__)

swagger_config = {
    "headers": [],
    "specs": [
        {
            "endpoint": "apispec_1",
            "route": "/apispec_1.json",
            "rule_filter": lambda rule: True,  # all in
            "model_filter": lambda tag: True,  # all in
        }
    ],
    "static_url_path": "/flasgger_static",
    "swagger_ui": True,
    "specs_route": "/swagger/",
    "title": "Kiosk Card API",
    "version": __version__,
    "description": __description__,
}

swagger = Swagger(app, config=swagger_config)

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('[kioskdispenser] %(asctime)s - %(levelname)s - %(message)s')
stream_handler = logging.StreamHandler(sys.stdout)
stream_handler.setFormatter(formatter)
logger.addHandler(stream_handler)


@app.errorhandler(404)
def page_not_found(e):
    """
    Global handler for 404 Not Found error.
    """
    return jsonify({"status": False}), 404


def flush_serial_port(serial_port):
    """
    Flushes the input and output buffers of the serial port.
    """
    serial_port.reset_input_buffer()
    serial_port.reset_output_buffer()


with app.app_context():
    app.config['VERSION'] = __version__
    app.config['ENV'] = "development"
    app.config['DEBUG'] = True
    app.config['TESTING'] = True
    app.config['dispenser_type'] = "MT166"
    app.config['dispenser_device'] = "/dev/ttyUSB0"
    app.config['dispenser_baud_rate'] = "9800"

    logger.info("Global variables initialization done")

# Connection settings with the device
if 'sphinx' in sys.modules or 'pdoc' in sys.modules:
    # If in documentation environment, use a mock
    ser = mock.Mock()
else:
    try:
        logger.info("Starting communication with MT166 Card Dispenser over %s with %s", app.config['dispenser_device'], app.config['dispenser_type'])
        ser = serial.Serial(
            port=app.config['dispenser_device'],
            baudrate=app.config['dispenser_baud_rate'],
            parity=serial.PARITY_NONE,
            bytesize=serial.EIGHTBITS,
            stopbits=serial.STOPBITS_ONE,
            timeout=1
        )
    except Exception as ex:
        logger.error("Error opening serial port: %s", str(ex))
        sys.exit(1)
